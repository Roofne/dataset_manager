![image.png](./image.png)

# Dataset Manager

## Objectif

A partir d'un fichier de données (csv ou json), générer un script sql ou un dictionnaire néccessaire à d'autres projets.

## Fonctionalités

- Choisir quelles données récupéré et sous quelle forme
- Generer une base de donnée à partir d'un fichier
- Generation à partir de fichiers multiples

## Logiciel Similaires

https://www.dropbase.io/post/5-ways-to-convert-csvs-to-databases-quickly

## Licence 

GNU v3
