# Librairies
import pandas as pd


# Fonctions
def load_dataset(args):
    if(args["name"]):
        f_name = args["name"]
        f_type = f_name.split(".")[1]
        f_path = "data/"
        
        if(f_type == "csv"):
            data = pd.read_csv(f_path+f_name)
        if(f_type == "json"):
            data = pd.read_json(f_path+f_name)
        return data    

def write_sql_command(file, cmd, data): 
    for i, row in data.iterrows():
        var = ""
        for key in data:
            var = var + str(row[key]) + ","
        file.write(str(cmd+"\n") % var)
        
def create_database(df):
    for key in df:
        l_data = len(df[key].unique())
        l_df = len(df[key])
        ratio = round(l_data / l_df, 3)
        print(str(key)+" : "+str(ratio))
        if ratio < 0.01:
            # Les données sont redontante, on créer une table pour ça
            1
            #write_sql_command(f, "CREATE TABLE %s (c_name c_type);")
            #write_sql_command(f, "INSERT INTO %s VALUES('%s');")
            #write_sql_command(f, "INSERT INTO main VALUES('%s');")
        else:
            # Les données ne sont pas redontantes, on le laisse dans la table principale
            # write_sql_command(f, "INSERT INTO Test VALUES('%s');", data)
            0
            
# Partie Principale
df = load_dataset({"name":"ds_salaries.csv"})
df.head()

f = open("script/demo.sql", "w")
data = df[["job_title", "salary_in_usd"]]
write_sql_command(f, "INSERT INTO Test VALUES('%s');", data)
f.close()

create_database(df)

# A faire
# [ ] load sql/json/csv
# [ ] dataset to database create/keys/values
# [ ] Analyse de la structure du fichier -> detection pattern
# [ ] measure slice speed (sur 100k values dataset)
# [ ] Test  
#       - pour des ligne aléatoire de taille aléatoire creer des commandes SQL
#       - random data to table splitting ?
